import xlsxwriter
import decimal
from math import sqrt

workbook = xlsxwriter.Workbook('TMatthias_Lab2.xlsx')
worksheet = workbook.add_worksheet()
letter_list = ["A", "A#", "B", "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#"]
init_num = 0
init_letter = 0
key_space_init = 4
A_0 = decimal.Decimal(27.5000)
sorted_freq_list = []
sorted_key_list = []

worksheet.write("A1", "Key Number")
worksheet.write("B1", "Note Name")
worksheet.write("C1", "Freq Value")
worksheet.write("D2", "Third Harmonic")
worksheet.write("D3", "Fifth Harmonic")
worksheet.write("E5", "Path Length (M) for 30 MS Delay")
worksheet.write("F5", "Path Length (M) for 50 MS Delay")

for i in range(0, 88):
    sorted_freq_list.append(round(float(A_0), 4))
    A_0 *= decimal.Decimal(2 ** (1 / 12))

for i in range(0, 88):
    if init_letter == 12:
        init_letter = 0
    letter = letter_list[init_letter]
    if letter == "C":
        init_num += 1
    init_letter += 1
    sorted_key_list.append(letter + str(init_num))

for i in range(0, 88):
    worksheet.write(i + 1, 0, i + 1)
    worksheet.write(i + 1, 1, sorted_key_list[i])
    worksheet.write(i + 1, 2, sorted_freq_list[i])

for i in range(77, 88):
    worksheet.write(0, key_space_init, sorted_key_list[i])
    key_space_init += 1
    worksheet.write(1, key_space_init, round(float(decimal.Decimal(sorted_freq_list[i] * 3)), 4))
    worksheet.write(2, key_space_init, round(float(decimal.Decimal(sorted_freq_list[i] * 5)), 4))

reflected_path_1 = (30 * (331 + (0.6 * 40))) + 10
reflected_path_2 = (50 * (331 + (0.6 * 40))) + 10

worksheet.write("E6", (reflected_path_1 / 1000))
worksheet.write("F6", (reflected_path_2 / 1000))

THD = (sqrt((0.5 ** 2 + 1.5 ** 2 + .25 ** 2 + .75 ** 2 + .25 ** 2)) / 10) * 100

worksheet.write("J5", "Total Harmonic Distortation")
worksheet.write("J6", THD)

workbook.close()
