import xlsxwriter

workbook = xlsxwriter.Workbook('TMatthias_Lab6.xlsx')
worksheet = workbook.add_worksheet()

Length = 28.54
Width = 22.91
Height = 10

Volume = Length * Width * Height
Area = 2*(Length * Height) + 2*(Width * Height)

s_per_f = 0.049

Sabines_125 = 0.6 * Area
Sabines_250 = 0.74 * Area
Sabines_500 = 0.88 * Area
Sabines_1000 = 0.96 * Area
Sabines_2000 = 0.93 * Area
Sabines_4000 = 0.85 * Area

RT60_125 = (s_per_f * Volume) / Sabines_125
RT60_250 = (s_per_f * Volume) / Sabines_250
RT60_500 = (s_per_f * Volume) / Sabines_500
RT60_1000 = (s_per_f * Volume) / Sabines_1000
RT60_2000 = (s_per_f * Volume) / Sabines_2000
RT60_4000 = (s_per_f * Volume) / Sabines_4000

worksheet.write("A1", "RT60 at 125Hz")
worksheet.write("B1", "RT60 at 250Hz")
worksheet.write("C1", "RT60 at 500Hz")
worksheet.write("D1", "RT60 at 1000Hz")
worksheet.write("E1", "RT60 at 2000Hz")
worksheet.write("E1", "RT60 at 4000Hz")

# string_form_125 = str("RT60 = 0.16 * (" + str(Volume) + "/" + str(Sabines_125)+")")
# string_form_250 = str("RT60 = 0.16 * (" + str(Volume) + "/" + str(Sabines_250)+")")
# string_form_500 = str("RT60 = 0.16 * (" + str(Volume) + "/" + str(Sabines_500)+")")
# string_form_1000 = str("RT60 = 0.16 * (" + str(Volume) + "/" + str(Sabines_1000)+")")
# string_form_2000 = str("RT60 = 0.16 * (" + str(Volume) + "/" + str(Sabines_2000)+")")
# string_form_4000 = str("RT60 = 0.16 * (" + str(Volume) + "/" + str(Sabines_4000)+")")

# worksheet.write("A2", string_form_125)
# worksheet.write("B2", string_form_250)
# worksheet.write("C2", string_form_500)
# worksheet.write("D2", string_form_1000)
# worksheet.write("E2", string_form_2000)
# worksheet.write("E2", string_form_4000)

worksheet.write("A3", RT60_125)
worksheet.write("B3", RT60_250)
worksheet.write("C3", RT60_500)
worksheet.write("D3", RT60_1000)
worksheet.write("E3", RT60_2000)
worksheet.write("E3", RT60_4000)

workbook.close()
