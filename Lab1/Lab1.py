import xlsxwriter

# Task1

workbook = xlsxwriter.Workbook('TMatthias_Lab1.xlsx')
worksheet = workbook.add_worksheet()
temp_C_list = []
speed_of_sound_list = []
sos_diff_list = []

temp_low_range = -40
temp_hi_range = 40
ref_temp = 20

for i in range(temp_low_range, temp_hi_range + 1):
    if i % 5 == 0:
        temp_C_list.append(i)

temp_C_list.reverse()

for i in range(temp_C_list.__len__()):
    speed_of_sound = 331 + (0.6 * temp_C_list[i])
    speed_of_sound_list.append(speed_of_sound)

for i in range(speed_of_sound_list.__len__()):
    sos_diff = speed_of_sound_list[i] - (331 + (0.6 * ref_temp))
    sos_diff_list.append(sos_diff)

print(temp_C_list)
print(speed_of_sound_list)
print(sos_diff_list)

worksheet.write('A1', 'Temperature')
worksheet.write('B1', 'Speed of Sound M/S')

for row in range(temp_C_list.__len__()):
    worksheet.write(row + 1, 0, temp_C_list[row])

for row in range(speed_of_sound_list.__len__()):
    worksheet.write(row + 1, 1, speed_of_sound_list[row])


def gen_sos_vs_temp_chart():
    sos_vs_temp_chart = workbook.add_chart({'type': 'line'})

    sos_vs_temp_chart.add_series({'name': '=Sheet1!$A$1',
                                  'categories': '=Sheet1!$A$2:$A$18',
                                  'values': '=Sheet1!$B$2:$B$18',
                                  'marker': {'type': 'diamond'},
                                  })
    sos_vs_temp_chart.set_title({'name': 'Speed of Sound (M/S) vs Temperature (C)'})
    sos_vs_temp_chart.set_x_axis({'name': 'Temperature'})
    sos_vs_temp_chart.set_y_axis({'name': 'Speed of Sound'})
    return sos_vs_temp_chart

worksheet.insert_chart('A20', gen_sos_vs_temp_chart())
workbook.close()
