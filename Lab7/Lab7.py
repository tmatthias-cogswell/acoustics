import xlsxwriter
from math import sqrt

workbook = xlsxwriter.Workbook('TMatthias_Lab7.xlsx')
worksheet = workbook.add_worksheet()

speed_of_sound = 331 + (0.6 * 20)
Length = 10
Width = 8
Height = 9
diff_1 = 0
diff_2 = 0
diff_3 = 0
jump_freq_1 = 0
jump_freq_2 = 0
jump_freq_3 = 0
i = 1

axial_freqs = []
tang_freqs = []
obliq_freqs = []
all_freqs = []

for p in range(0, 5):
    for q in range(0, 5):
        for r in range(0, 5):
            p_val = (p / Length) ** 2
            q_val = (q / Width) ** 2
            r_val = (r / Height) ** 2
            freq = round((speed_of_sound / 2) * sqrt(p_val + q_val + r_val), 4)
            worksheet.write(i, 0, (str(p) + ", " + str(q) + ", " + str(r)))
            worksheet.write(i, 1, (freq))
            if (p != 0 and q == 0 and r == 0) or (p == 0 and q != 0 and r == 0) or (p == 0 and q == 0 and r != 0):
                worksheet.write(i, 2, 1)
                axial_freqs.append(freq)
            elif (p != 0 and q != 0 and r == 0) or (p != 0 and q == 0 and r != 0) or (p == 0 and q != 0 and r != 0):
                worksheet.write(i, 2, 2)
                tang_freqs.append(freq)
            elif p != 0 and q != 0 and r != 0:
                worksheet.write(i, 2, 3)
                obliq_freqs.append(freq)
            else:
                worksheet.write(i, 2, "None")
            all_freqs.append(freq)
            i += 1

axial_freqs.sort()
tang_freqs.sort()
obliq_freqs.sort()
all_freqs.pop(0)
all_freqs.sort()

for j in range(all_freqs.__len__()):
    if ((all_freqs[j] - all_freqs[j - 1]) > diff_1):
        diff_3 = diff_2
        diff_2 = diff_1
        diff_1 = all_freqs[j] - all_freqs[j - 1]
        jump_freq_3 = jump_freq_2
        jump_freq_2 = jump_freq_1
        jump_freq_1 = all_freqs[j]

worksheet.write(0, 0, "Mode")
worksheet.write(0, 1, "Modal Frequency")

worksheet.write(0, 4, "Sorted Axials")
for i in range(axial_freqs.__len__()):
    worksheet.write((i + 1), 4, axial_freqs[i])

worksheet.write(0, 5, "Sorted Tangentials")
for i in range(tang_freqs.__len__()):
    worksheet.write((i + 1), 5, tang_freqs[i])

worksheet.write(0, 6, "Sorted Obliques")
for i in range(obliq_freqs.__len__()):
    worksheet.write((i + 1), 6, obliq_freqs[i])

worksheet.write(0, 7, "All Frequencies")
for i in range(all_freqs.__len__()):
    worksheet.write((i + 1), 7, all_freqs[i])

worksheet.write(0, 8, "Problem Frequencies Due to Gaps Near")
worksheet.write(1, 8, jump_freq_1)
worksheet.write(2, 8, jump_freq_2)
worksheet.write(3, 8, jump_freq_3)

def gen_mode_chart():
    mode_chart = workbook.add_chart({'type': 'line'})

    mode_chart.add_series({'name': '=Sheet1!$E$1',
                           'values': '=Sheet1!$E$2:$E$13',
                           })
    mode_chart.add_series({'name': '=Sheet1!$F$1',
                           'values': '=Sheet1!$F$2:$F$49',
                           })
    mode_chart.add_series({'name': '=Sheet1!$G$1',
                           'values': '=Sheet1!$G$2:$G$65',
                           })
    return mode_chart


def gen_mode_chart_2():
    mode_chart = workbook.add_chart({'type': 'line'})

    mode_chart.add_series({'name': '=Sheet1!$H$1',
                           'values': '=Sheet1!$H$2:$H$125',
                           })
    return mode_chart


worksheet.insert_chart('K1', gen_mode_chart())
worksheet.insert_chart('K20', gen_mode_chart_2())

workbook.close()
