import xlsxwriter
from math import sqrt

workbook = xlsxwriter.Workbook('TMatthias_Lab11.xlsx')
worksheet = workbook.add_worksheet()

A_Glasses = [(1 / 4), (1 / 4), (1 / 2), (1 / 2), (3 / 4), (1)]
B_Glasses = [(1 / 4), (1 / 2), (1 / 2), (3 / 4), (1), (1)]
D1 = 0.5
D2 = 1
D3 = 2
D4 = 4
D5 = 8
D6 = 12

MAM_LIST_1 = []
MAM_LIST_2 = []
MAM_LIST_3 = []
MAM_LIST_4 = []
MAM_LIST_5 = []
MAM_LIST_6 = []


def gvd_ft3_to_gsd_ft2(gvd_ft, thickness):
    gvd_in3 = gvd_ft / (12 * 12 * 12)
    gsd_in2 = gvd_in3 / thickness
    gsd_ft2 = gsd_in2 * 144
    return gsd_ft2


def get_mass_air_resonance(m1, m2, d):
    mam = 170 * sqrt(((m1 + m2) / (m1 * m2 * d)))
    return mam


m1 = gvd_ft3_to_gsd_ft2(156, A_Glasses[0])
m2 = gvd_ft3_to_gsd_ft2(156, B_Glasses[0])

MAM_LIST_1.append(get_mass_air_resonance(m1, m2, D1))
MAM_LIST_1.append(get_mass_air_resonance(m1, m2, D2))
MAM_LIST_1.append(get_mass_air_resonance(m1, m2, D3))
MAM_LIST_1.append(get_mass_air_resonance(m1, m2, D4))
MAM_LIST_1.append(get_mass_air_resonance(m1, m2, D5))
MAM_LIST_1.append(get_mass_air_resonance(m1, m2, D6))

m1 = gvd_ft3_to_gsd_ft2(156, A_Glasses[1])
m2 = gvd_ft3_to_gsd_ft2(156, B_Glasses[1])

MAM_LIST_2.append(get_mass_air_resonance(m1, m2, D1))
MAM_LIST_2.append(get_mass_air_resonance(m1, m2, D2))
MAM_LIST_2.append(get_mass_air_resonance(m1, m2, D3))
MAM_LIST_2.append(get_mass_air_resonance(m1, m2, D4))
MAM_LIST_2.append(get_mass_air_resonance(m1, m2, D5))
MAM_LIST_2.append(get_mass_air_resonance(m1, m2, D6))

m1 = gvd_ft3_to_gsd_ft2(156, A_Glasses[2])
m2 = gvd_ft3_to_gsd_ft2(156, B_Glasses[2])

MAM_LIST_3.append(get_mass_air_resonance(m1, m2, D1))
MAM_LIST_3.append(get_mass_air_resonance(m1, m2, D2))
MAM_LIST_3.append(get_mass_air_resonance(m1, m2, D3))
MAM_LIST_3.append(get_mass_air_resonance(m1, m2, D4))
MAM_LIST_3.append(get_mass_air_resonance(m1, m2, D5))
MAM_LIST_3.append(get_mass_air_resonance(m1, m2, D6))

m1 = gvd_ft3_to_gsd_ft2(156, A_Glasses[3])
m2 = gvd_ft3_to_gsd_ft2(156, B_Glasses[3])

MAM_LIST_4.append(get_mass_air_resonance(m1, m2, D1))
MAM_LIST_4.append(get_mass_air_resonance(m1, m2, D2))
MAM_LIST_4.append(get_mass_air_resonance(m1, m2, D3))
MAM_LIST_4.append(get_mass_air_resonance(m1, m2, D4))
MAM_LIST_4.append(get_mass_air_resonance(m1, m2, D5))
MAM_LIST_4.append(get_mass_air_resonance(m1, m2, D6))

m1 = gvd_ft3_to_gsd_ft2(156, A_Glasses[4])
m2 = gvd_ft3_to_gsd_ft2(156, B_Glasses[4])

MAM_LIST_5.append(get_mass_air_resonance(m1, m2, D1))
MAM_LIST_5.append(get_mass_air_resonance(m1, m2, D2))
MAM_LIST_5.append(get_mass_air_resonance(m1, m2, D3))
MAM_LIST_5.append(get_mass_air_resonance(m1, m2, D4))
MAM_LIST_5.append(get_mass_air_resonance(m1, m2, D5))
MAM_LIST_5.append(get_mass_air_resonance(m1, m2, D6))

m1 = gvd_ft3_to_gsd_ft2(156, A_Glasses[5])
m2 = gvd_ft3_to_gsd_ft2(156, B_Glasses[5])

MAM_LIST_6.append(get_mass_air_resonance(m1, m2, D1))
MAM_LIST_6.append(get_mass_air_resonance(m1, m2, D2))
MAM_LIST_6.append(get_mass_air_resonance(m1, m2, D3))
MAM_LIST_6.append(get_mass_air_resonance(m1, m2, D4))
MAM_LIST_6.append(get_mass_air_resonance(m1, m2, D5))
MAM_LIST_6.append(get_mass_air_resonance(m1, m2, D6))

print(MAM_LIST_1)
print(MAM_LIST_2)
print(MAM_LIST_3)
print(MAM_LIST_4)
print(MAM_LIST_5)
print(MAM_LIST_6)

for i in range(0,6):
    worksheet.write(1, i+1, MAM_LIST_1[i])
    worksheet.write(2, i+1, MAM_LIST_2[i])
    worksheet.write(3, i+1, MAM_LIST_3[i])
    worksheet.write(4, i+1, MAM_LIST_4[i])
    worksheet.write(5, i+1, MAM_LIST_5[i])
    worksheet.write(6, i+1, MAM_LIST_6[i])

worksheet.write("A2", "1/4 and 1/4")
worksheet.write("A3", "1/4 and 1/2")
worksheet.write("A4", "1/2 and 1/2")
worksheet.write("A5", "1/2 and 3/4")
worksheet.write("A6", "3/4 and 1")
worksheet.write("A7", "1 and 1")
worksheet.write("B1", "d1")
worksheet.write("C1", "d2")
worksheet.write("D1", "d3")
worksheet.write("E1", "d4")
worksheet.write("F1", "d5")
worksheet.write("G1", "d6")

workbook.close()
