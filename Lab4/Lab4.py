import xlsxwriter

workbook = xlsxwriter.Workbook('TMatthias_Lab4.xlsx')
worksheet = workbook.add_worksheet()

room_volume = 324
wall_area = 122
wall_area_average_sound_coef = 0.03
ceiling_area = 98
ceiling_area_average_sound_coef = 0.06

total_average_sound_coef = ((wall_area * wall_area_average_sound_coef) * (
ceiling_area * ceiling_area_average_sound_coef)) / (wall_area + ceiling_area)
reverb_time = (0.161 * room_volume) / total_average_sound_coef

worksheet.write("B15", "Average Sound Coef")
worksheet.write("B16", total_average_sound_coef)
worksheet.write("D15", "Reverb Time")
worksheet.write("D16", reverb_time)

humidity = list()
f = list()
air_att_list = list()
final_dict = dict()

for i in range(1500, 10001):
    if i % 500 == 0:
        f.append(i)

for i in range(20, 71):
    if i % 5 == 0:
        humidity.append(i)

for i in range(f.__len__()):
    for j in range(humidity.__len__()):
        air_attenuation_coef = 0.00055 * (50 / humidity[j]) * ((f[i] / 1000) ** 1.7)
        air_att_list.append(round(air_attenuation_coef, 10))

    dict_position = f[i]
    final_dict[dict_position] = air_att_list
    air_att_list = list()

worksheet.write('A1', "Air Atten Coef's")
worksheet.write('A2', 'Humidity')
worksheet.merge_range('B1:S1', 'Frequency')

for row in range(humidity.__len__()):
    worksheet.write(row + 2, 0, humidity[row])

for row in range(f.__len__()):
    worksheet.write(1, row + 1, f[row])

for i in range(f.__len__()):
    dict_list_iter = sorted(final_dict.keys())
    temp_freq = dict_list_iter[i]
    temp_coef = final_dict[temp_freq]
    for j in range(humidity.__len__()):
        worksheet.write(j + 2, i + 1, temp_coef[j])


def gen_chart():
    chart = workbook.add_chart({'type': 'line'})

    letters = ['B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S']

    for i in range(letters.__len__()):
            chart.add_series({'name': '=Sheet1!$'+letters[i]+'$2',
                      'categories': '=Sheet1!$A$3:$A$13',
                      'values': '=Sheet1!$'+letters[i]+'$3:$'+letters[i]+'$13',
                      'marker': {'type': 'diamond'},
                      })

    chart.set_title({'name': 'Air Atten Coef'})
    chart.set_x_axis({'name': 'Humidity'})
    return chart


worksheet.insert_chart('F20', gen_chart())
workbook.close()
